import { Container } from "pixi.js";
import { Board } from "./Components/Board";
import { IScene } from "./Manager";

export class GameScene extends Container implements IScene {
  private board: Board = new Board();
  constructor() {
    super();
    this.addChild(this.board);
  }
  public update(): void {
    
  }
}
