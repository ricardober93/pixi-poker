import { IScene } from "./../Manager";
import { Container, Graphics, Text } from "pixi.js";

type textTableI = {
  text: string;
  x: number;
  y: number;
  zIndex: number;
};
new Text("Escalera real", {
  fill: "#000",
  fontSize: 16,
});

export class Board extends Container implements IScene {
  private background: Graphics = new Graphics();

  private distanceX = 60
  private distanceY = 25
  private rowFrist: textTableI[] = [
    { text: "Escalera Real", x: this.distanceX, y: this.distanceY, zIndex: 10 },
    { text: "2400", x: this.distanceX * 4.5, y: this.distanceY, zIndex: 10 },
    { text: "2800", x: this.distanceX * 6, y: this.distanceY, zIndex: 10 },
    { text: "3200", x: this.distanceX * 7.5, y: this.distanceY, zIndex: 10 },
    { text: "3600", x: this.distanceX * 9, y: this.distanceY, zIndex: 10 },
    { text: "8000", x: this.distanceX * 10.5, y: this.distanceY, zIndex: 10 },
    { text: "Escalera Color", x: this.distanceX, y: this.distanceY *2, zIndex: 10 },
    { text: "300", x: this.distanceX * 4.5, y: this.distanceY *2, zIndex: 10 },
    { text: "350", x: this.distanceX * 6, y: this.distanceY *2, zIndex: 10 },
    { text: "400", x: this.distanceX * 7.5, y: this.distanceY *2, zIndex: 10 },
    { text: "450", x: this.distanceX * 9, y: this.distanceY *2, zIndex: 10 },
    { text: "500", x: this.distanceX * 10.5, y: this.distanceY *2, zIndex: 10 },
    { text: "Poker", x: this.distanceX, y: this.distanceY *3, zIndex: 10 },
    { text: "150", x: this.distanceX * 4.5, y: this.distanceY *3, zIndex: 10 },
    { text: "175", x: this.distanceX * 6, y: this.distanceY *3, zIndex: 10 },
    { text: "200", x: this.distanceX * 7.5, y: this.distanceY *3, zIndex: 10 },
    { text: "225", x: this.distanceX * 9, y: this.distanceY *3, zIndex: 10 },
    { text: "250", x: this.distanceX * 10.5, y: this.distanceY *3, zIndex: 10 },
    { text: "Full", x: this.distanceX, y: this.distanceY *4, zIndex: 10 },
    { text: "36", x: this.distanceX * 4.5, y: this.distanceY *4, zIndex: 10 },
    { text: "42", x: this.distanceX * 6, y: this.distanceY *4, zIndex: 10 },
    { text: "48", x: this.distanceX * 7.5, y: this.distanceY *4, zIndex: 10 },
    { text: "54", x: this.distanceX * 9, y: this.distanceY *4, zIndex: 10 },
    { text: "60", x: this.distanceX * 10.5, y: this.distanceY *4, zIndex: 10 },
    { text: "Color", x: this.distanceX, y: this.distanceY *5, zIndex: 10 },
    { text: "30", x: this.distanceX * 4.5, y: this.distanceY *5, zIndex: 10 },
    { text: "35", x: this.distanceX * 6, y: this.distanceY *5, zIndex: 10 },
    { text: "40", x: this.distanceX * 7.5, y: this.distanceY *5, zIndex: 10 },
    { text: "45", x: this.distanceX * 9, y: this.distanceY *5, zIndex: 10 },
    { text: "50", x: this.distanceX * 10.5, y: this.distanceY *5, zIndex: 10 },
    { text: "Escalera", x: this.distanceX, y: this.distanceY*6, zIndex: 10 },
    { text: "24", x: this.distanceX * 4.5, y: this.distanceY*6, zIndex: 10 },
    { text: "28", x: this.distanceX * 6, y: this.distanceY*6, zIndex: 10 },
    { text: "32", x: this.distanceX * 7.5, y: this.distanceY*6, zIndex: 10 },
    { text: "36", x: this.distanceX * 9, y: this.distanceY*6, zIndex: 10 },
    { text: "40", x: this.distanceX * 10.5, y: this.distanceY*6, zIndex: 10 },
    { text: "Terna", x: this.distanceX, y: this.distanceY *7, zIndex: 10 },
    { text: "18", x: this.distanceX * 4.5, y: this.distanceY *7, zIndex: 10 },
    { text: "21", x: this.distanceX * 6, y: this.distanceY *7, zIndex: 10 },
    { text: "24", x: this.distanceX * 7.5, y: this.distanceY *7, zIndex: 10 },
    { text: "27", x: this.distanceX * 9, y: this.distanceY *7, zIndex: 10 },
    { text: "30", x: this.distanceX * 10.5, y: this.distanceY *7, zIndex: 10 },
    { text: "Doble Par", x: this.distanceX, y: this.distanceY *8, zIndex: 10 },
    { text: "12", x: this.distanceX * 4.5, y: this.distanceY *8, zIndex: 10 },
    { text: "14", x: this.distanceX * 6, y: this.distanceY *8, zIndex: 10 },
    { text: "16", x: this.distanceX * 7.5, y: this.distanceY *8, zIndex: 10 },
    { text: "18", x: this.distanceX * 9, y: this.distanceY *8, zIndex: 10 },
    { text: "20", x: this.distanceX * 10.5, y: this.distanceY *8, zIndex: 10 },
    { text: "Jotas o Mejor", x: this.distanceX, y: this.distanceY *9, zIndex: 10 },
    { text: "6", x: this.distanceX * 4.5, y: this.distanceY *9, zIndex: 10 },
    { text: "7", x: this.distanceX * 6, y: this.distanceY *9, zIndex: 10 },
    { text: "8", x: this.distanceX * 7.5, y: this.distanceY *9, zIndex: 10 },
    { text: "9", x: this.distanceX * 9, y: this.distanceY *9, zIndex: 10 },
    { text: "10", x: this.distanceX * 10.5, y: this.distanceY *9, zIndex: 10 }
];
  constructor() {
    super();

    this.background.beginFill(0xffffff);
    this.background.drawRect(50, 20, 650, 230);
    this.background.endFill();
    this.background.zIndex = 0;
    this.addChild(this.background);

    this.rowFrist.forEach(element => {
        const text = new Text( element.text, {
            fill: "#000",
            fontSize: 16,
          });
        text.position.x = element.x
        text.position.y = element.y
        text.zIndex = element.zIndex

        this.addChild(text)
    });
  }

  public update(): void {}
}
